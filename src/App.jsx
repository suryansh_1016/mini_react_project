import './App.css'
import { useEffect, useState } from 'react';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import ImageTiles from './components/ImageTiles';

function App() {

  // ZLKqX72046D74zJmyRa1YNLJAM4WHCoPnHv4zoPr1n7ggXnqJAtZZSwq

  let [data, setData] = useState(null);

  let fetchData = async (keyword) => {
    let res = await fetch(`https://api.pexels.com/v1/search?query=${keyword}`,
      {
        headers: {
          'Authorization': 'ZLKqX72046D74zJmyRa1YNLJAM4WHCoPnHv4zoPr1n7ggXnqJAtZZSwq'
        }
      });
    let resdata = await res.json();
    // console.log(resdata);
    setData(resdata);
  }

  useEffect(() => {
    fetchData("popular")
  }, [])

  return (
    <>
      <Navbar fetchData={fetchData} />

      <div className='home-container flex , flex-wrap justify-around  gap-[2rem] h-[fit-content] w-[99vw] p-[20px] '>
        {data != null &&
          data.photos.map(element => {
            return <ImageTiles imageSrc={element.src.large} imgId={element.id} />
          })
        }
      </div>

      <Footer />
    </>
  )
}

export default App
