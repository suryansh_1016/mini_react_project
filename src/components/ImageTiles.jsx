import React from 'react'
import '../App.css'

const ImageTiles = ({ imageSrc, imgId }) => {
  return (
    <>
      <div className='image-card' style={{backgroundImage: `url('${imageSrc}')`}}>
      </div>
    </>
  )
}
export default ImageTiles