import React, { useState } from 'react'

const Navbar = ({ fetchData }) => {

  let [search, setSearch] = useState("");

  return (
    <>

      <nav
        className="flex w-full flex-nowrap items-center justify-between bg-zinc-50 py-2 text-neutral-500 shadow-dark-mild hover:text-neutral-700 focus:text-neutral-700 dark:bg-neutral-700 lg:flex-wrap lg:justify-start lg:py-4"
        data-twe-navbar-ref>
        <div className="flex w-full flex-wrap items-center justify-between px-3">
          <div className="ms-2">
            <a className="text-4xl text-black dark:text-white" href="#">Blazingwalls</a>
          </div>
          <div className="search flex gap-">
            <input
              className="w-full max-w-sm h-12 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:ring-2 focus:ring-gray-400 focus:ring-opacity-50 transition duration-300 ease-in-out hover:border-gray-400"
              placeholder="Search"
              type="text"
              name="text"

              onChange={(e) => {
                setSearch(e.target.value)
              }}
            />
            <button className=' bg-blue-600 text-white p-3 rounded-md mx-2' onClick={() => {
              fetchData(search);
            }}>search</button>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Navbar